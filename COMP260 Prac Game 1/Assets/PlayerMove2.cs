﻿using UnityEngine;
using System.Collections;

public class PlayerMove2 : MonoBehaviour {

	// Use this for initialization
	void Start()
	{

	}
	public float maxSpeed = 5.0f;

	// Update is called once per frame
	void Update()
	{
		Vector2 direction;
		direction.x = Input.GetAxis("Horizontal2");
		direction.y = Input.GetAxis("Vertical2");

		// scale by the maxSpeed parameter
		Vector2 velocity = direction * maxSpeed;

		// move the object
		transform.Translate(velocity * Time.deltaTime);

	}
}
