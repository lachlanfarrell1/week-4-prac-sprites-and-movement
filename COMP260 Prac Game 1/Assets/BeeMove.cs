﻿using UnityEngine;
using System.Collections;

public class BeeMove : MonoBehaviour {

	void Start() {
	}

	public float speed = 4.0f;        // metres per second
	public float turnSpeed = 180.0f;  // degrees per second
	public Transform target1;
	public Transform target2;
	public Vector2 heading = Vector3.right;

	void Update() {
		Vector2 direction;
		if (target1.position.magnitude > target2.position.magnitude) {
			direction = target2.position - transform.position;
		} 
		else {
			direction = target1.position - transform.position;
		}


		// calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;

		// turn left or right
		if (direction.IsOnLeft(heading)) {

			// target on left, rotate anticlockwise
			heading = heading.Rotate(angle);
		}

		else {
			// target on right, rotate clockwise
			heading = heading.Rotate(-angle);
		}

		transform.Translate(heading * speed * Time.deltaTime);
	}


	void OnDrawGizmos() {
		// draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);

		// draw target vector in yellow
		Gizmos.color = Color.yellow;
		Vector2 direction;

		if (target1.position.magnitude > target2.position.magnitude){
			direction = target2.position - transform.position;
		}

		else {
			direction = target1.position - transform.position;
		}

		Gizmos.DrawRay(transform.position, direction);
	}
}
